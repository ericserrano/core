create or replace package XXSTAR_ROR_REL_SHIPMENTS_N2 is
  --============================================================================================
  --Description:This script is registered as conc program in oracle applications.
  --            This fetches outstanding SHIP information ,
  --               creates *.txt files in unix using UTL_file package.
  --            These files are later on FTPd to UPS

  --Developer: Eric S.

  --History   :
  -- Change Date            Change by                 Change Made
  ------------           ----------               --------------------------------------
  -- 11/22/2016             Eric S.                Initial Creation
  --============================================================================================
  PROCEDURE main(out_errbuf_s OUT VARCHAR2,
                 out_errnum_n OUT NUMBER,
                 
                 p_date_from   in varchar2,
                 p_date_to     in varchar2,
                 p_org_id      in varchar2,
                 p_delivery_id in varchar2,
                 --arguments sftp credentials (Lookup Type)
                 p_sftp_credentials in varchar2,
                 --arguments to upload the file Y/N
                 p_upload_file in varchar2,
                 --arguments file destination
                 p_dir_to in varchar2);

  PROCEDURE mntrng(out_errbuf_s OUT VARCHAR2,
                   out_errnum_n OUT NUMBER,
                   
                   p_date_from   in varchar2,
                   p_date_to     in varchar2,
                   p_org_id      in varchar2,
                   p_delivery_id in varchar2,
                   
                   p_sftp_request_id in number,
                   p_conc_request_id in number);

  function get_unit_sell_price(p_line_id in number) return number;
  function gt_shp_mthd(p_item_id in number) return varchar2;
  function gt_chk_wrrnt_so(p_order_type         in varchar2,
                           p_unit_selling_price in number) return number;
  FUNCTION F_Get_STATE(p_ship_to_site_code_id IN NUMBER) RETURN VARCHAR2;
end XXSTAR_ROR_REL_SHIPMENTS_N2;
/
create or replace package body XXSTAR_ROR_REL_SHIPMENTS_N2 is
  --===================================================================================
  --Description:This script is registered as conc program in oracle applications.
  --            This fetches outstanding SHIP information ,
  --               creates *.txt file in unix using UTL_file package.
  --            This file is later on FTPd to UPS .

  --Developer: Eric S.

  --History   :
  -- Change Date            Change by                 Change Made
  ------------           ----------               --------------------------------------
  -- 11/22/2016             Eric S.                Initial Creation
  --===================================================================================
  PROCEDURE main(out_errbuf_s OUT VARCHAR2,
                 out_errnum_n OUT NUMBER,
                 
                 p_date_from   in varchar2,
                 p_date_to     in varchar2,
                 p_org_id      in varchar2,
                 p_delivery_id in varchar2,
                 --arguments sftp credentials (Lookup Type)
                 p_sftp_credentials in varchar2,
                 --arguments to upload the file Y/N
                 p_upload_file in varchar2,
                 --arguments file destination
                 p_dir_to in varchar2) IS
    --shipments cursor
    cursor c_shpmts is
      select wdd.DELIVERY_DETAIL_ID,
             replace(to_char(wnd.creation_date, 'DD-MM-YYYY'), '"', '""') as release_date,
             replace(wnd.delivery_id, '"', '""') as delivery_number,
             replace(nvl(wdd.ship_method_code,
                         gt_shp_mthd(ol.INVENTORY_ITEM_ID)),
                     '"',
                     '""') as ship_via,
             replace(to_char(wdd.date_scheduled, 'DD-MM-YYYY'), '"', '""') as start_ship_date,
             SUBSTR(replace(replace(replace(ship_hp.party_name,
                                            CHR(13),
                                            ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    35) as ship_to_name,
             replace(ship_ca.account_number, '"', '""') as ship_to_id,
             SUBSTR(replace(replace(replace(ship_hl.address1, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as ship_address1,
             SUBSTR(replace(replace(replace(ship_hl.address2, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as ship_address2,
             SUBSTR(replace(replace(replace(ship_hl.address3, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as ship_address3,
             replace(ship_hl.city, '"', '""') as ship_city,
             SUBSTR(replace(ship_hl.postal_code, '"', '""'), 1, 11) as ship_postal_code,
             replace(ship_hl.country, '"', '""') as ship_country,
             replace((TO_CHAR(ol.line_number) ||
                     DECODE(ol.shipment_number,
                             NULL,
                             NULL,
                             '.' || TO_CHAR(ol.shipment_number)) ||
                     DECODE(ol.option_number,
                             NULL,
                             NULL,
                             '.' || TO_CHAR(ol.option_number)) ||
                     DECODE(ol.component_number,
                             NULL,
                             NULL,
                             DECODE(ol.option_number, NULL, '.', NULL) || '.' ||
                             TO_CHAR(ol.component_number)) ||
                     DECODE(ol.service_number,
                             NULL,
                             NULL,
                             DECODE(ol.component_number, NULL, '.', NULL) ||
                             DECODE(ol.option_number, NULL, '.', NULL) || '.' ||
                             TO_CHAR(ol.service_number))),
                     '"',
                     '""') as line_number,
             replace(ol.ordered_item, '"', '""') as item_number,
             replace(decode(nvl(mut.SERIAL_NUMBER, msnt.FM_SERIAL_NUMBER),
                            null,
                            wdd.REQUESTED_QUANTITY,
                            1),
                     '"',
                     '""') as shipped_quantity, --updtd
             replace(oh.order_number, '"', '""') as order_number,
             replace(nvl(wnd.currency_code, wdd.currency_code), '"', '""') as currency_code,
             replace(decode(gt_chk_wrrnt_so(ott.name,
                                            nvl(ol.UNIT_SELLING_PRICE, 0)),
                            1,
                            1,
                            get_unit_sell_price(ol.line_id)),
                     '"',
                     '""') as unit_selling_price, --tbch
             replace('Star Trac', '"', '""') as Business_unit,
             replace(mtrl.from_subinventory_code, '"', '""') as subinventory_code,
             replace(oh.freight_terms_code, '"', '""') as inco_terms,
             replace(ol.freight_terms_code, '"', '""') as freight_terms,
             SUBSTR(replace(bill_hp.party_name, '"', '""'), 1, 35) as bill_to_name,
             replace(bill_ca.account_number, '"', '""') as bill_to_id,
             SUBSTR(replace(replace(replace(bill_hl.address1, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as bill_address1,
             SUBSTR(replace(replace(replace(bill_hl.address2, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as bill_address2,
             SUBSTR(replace(replace(replace(bill_hl.address3, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as bill_address3,
             replace(bill_hl.city, '"', '""') as bill_city,
             SUBSTR(replace(bill_hl.postal_code, '"', '""'), 1, 11) as bill_postal_code,
             replace(bill_hl.country, '"', '""') as bill_country,
             SUBSTR(replace(del_hp.party_name, '"', '""'), 1, 35) as del_to_name,
             replace(del_ca.account_number, '"', '""') as del_to_id,
             SUBSTR(replace(replace(replace(del_hl.address1, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as del_address1,
             SUBSTR(replace(replace(replace(del_hl.address2, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as del_address2,
             SUBSTR(replace(replace(replace(del_hl.address3, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    72) as del_address3,
             replace(del_hl.city, '"', '""') as del_city,
             SUBSTR(replace(del_hl.postal_code, '"', '""'), 1, 11) as del_postal_code,
             replace(del_hl.country, '"', '""') as del_country,
             replace(decode(gt_chk_wrrnt_so(ott.name,
                                            nvl(ol.UNIT_SELLING_PRICE, 0)),
                            1,
                            1,
                            nvl(decode(nvl(mut.SERIAL_NUMBER,
                                           msnt.FM_SERIAL_NUMBER),
                                       null,
                                       wdd.shipped_quantity,
                                       1) * get_unit_sell_price(ol.line_id),
                                0)),
                     '"',
                     '""') as Extended_price, --tbch
             SUBSTR(replace(REPLACE(replace(oh.cust_po_number, CHR(13), ' '),
                                    CHR(10),
                                    ' '),
                            '"',
                            '""'),
                    1,
                    26) as cust_po_number,
             --added 11-23-16
             replace(REPLACE(replace(ol.ATTRIBUTE1, CHR(13), ' '),
                             CHR(10),
                             ' '),
                     '"',
                     '""') as line_comments,
             replace('UPS', '"', '""') as TRAD_PARTN_ID,
             replace((select wpb.name
                       from WSH_PICKING_BATCHES wpb
                      where wpb.BATCH_ID = wnd.BATCH_ID),
                     '"',
                     '""') as batch_num,
             replace(REPLACE(replace(nvl(mut.SERIAL_NUMBER,
                                         msnt.FM_SERIAL_NUMBER),
                                     CHR(13),
                                     ' '),
                             CHR(10),
                             ' '),
                     '"',
                     '""') as serial_number,
             replace((ship_party.PERSON_LAST_NAME ||
                     DECODE(ship_party.PERSON_FIRST_NAME,
                             NULL,
                             NULL,
                             ', ' || ship_party.PERSON_FIRST_NAME)),
                     '"',
                     '""') as shp_cntc_name,
             replace(party.email_address, '"', '""') as shp_cntc_email,
             replace((SELECT HCP.PHONE_AREA_CODE || ' ' || HCP.PHONE_NUMBER
                       FROM HZ_CONTACT_POINTS HCP
                      WHERE HCP.CONTACT_POINT_ID = OH.SOLD_TO_PHONE_ID
                        AND HCP.CONTACT_POINT_TYPE = 'PHONE'
                        AND HCP.OWNER_TABLE_NAME = 'HZ_PARTY_SITES'),
                     '"',
                     '""') as ship_cntc_phone,
             replace((F_Get_STATE(oh.SHIP_TO_ORG_ID)), '"', '""') as ship_to_state,
             replace(ol.ORDER_QUANTITY_UOM, '"', '""') as OrderQtyUOM,
             replace(ol.FLOW_STATUS_CODE, '"', '""') as OrdersStatusCode,
             replace(ott.name, '"', '""') as OrderType,
             replace(micv.category_concat_segs, '"', '""') as ItemTypeCategory,
             --Values To Filter the Staging Table
             replace(wnd.creation_date, '"', '""') as delivery_creation_date,
             replace(ol.ship_from_org_id, '"', '""') as order_line_ship_from_org_id,
             replace(wnd.delivery_id, '"', '""') as wnd_delivery_id
        from hz_parties              party,
             hz_cust_accounts        cust_acct,
             oe_transaction_types_tl ott,
             wsh_new_deliveries      wnd
        join wsh_delivery_assignments wda
          on wnd.delivery_id = wda.delivery_id
        join wsh_delivery_details wdd
          on wda.delivery_detail_id = wdd.delivery_detail_id
        join oe_order_headers_all oh
          on wdd.source_header_id = oh.header_id
        left join oe_order_lines_all ol
          on wdd.source_line_id = ol.line_id
        join(hz_cust_site_uses_all ship_csu
        join hz_cust_acct_sites_all ship_cas
          on ship_csu.cust_acct_site_id = ship_cas.cust_acct_site_id
        join hz_cust_accounts_all ship_ca
          on ship_cas.cust_account_id = ship_ca.cust_account_id
        join hz_parties ship_hp
          on ship_ca.party_id = ship_hp.party_id
        join hz_party_sites ship_hps
          on ship_cas.party_site_id = ship_hps.party_site_id
        join hz_locations ship_hl
          on ship_hps.location_id = ship_hl.location_id) on ship_csu.site_use_id = oh.ship_to_org_id
        join(hz_cust_site_uses_all bill_csu
        join hz_cust_acct_sites_all bill_cas
          on bill_csu.cust_acct_site_id = bill_cas.cust_acct_site_id
        join hz_cust_accounts_all bill_ca
          on bill_cas.cust_account_id = bill_ca.cust_account_id
        join hz_parties bill_hp
          on bill_ca.party_id = bill_hp.party_id
        join hz_party_sites bill_hps
          on bill_cas.party_site_id = bill_hps.party_site_id
        join hz_locations bill_hl
          on bill_hps.location_id = bill_hl.location_id) on bill_csu.site_use_id = oh.ship_to_org_id
        left join(hz_cust_site_uses_all del_csu
        join hz_cust_acct_sites_all del_cas
          on del_csu.cust_acct_site_id = del_cas.cust_acct_site_id
        join hz_cust_accounts_all del_ca
          on del_cas.cust_account_id = del_ca.cust_account_id
        join hz_parties del_hp
          on del_ca.party_id = del_hp.party_id
        join hz_party_sites del_hps
          on del_cas.party_site_id = del_hps.party_site_id
        join hz_locations del_hl
          on del_hps.location_id = del_hl.location_id) on del_csu.site_use_id = nvl(oh.deliver_to_org_id, oh.ship_to_org_id)
        left join mtl_txn_request_lines mtrl
          on wdd.move_order_line_id = mtrl.line_id
      --join added 24-11-16
        left outer join hz_cust_account_roles ship_roles
          on (oh.ship_to_contact_id = ship_roles.cust_account_role_id)
         and ('CONTACT' = ship_roles.role_type)
        left outer join hz_relationships ship_rel
          on (ship_roles.party_id = ship_rel.party_id)
        left outer join hz_cust_accounts ship_acct
          on (ship_roles.cust_account_id = ship_acct.cust_account_id)
        left outer join hz_parties ship_party
          on (ship_rel.subject_id = ship_party.party_id)
      --get item type
        left outer join mtl_item_categories_v micv
          on (ol.inventory_item_id = micv.inventory_item_id)
      --serial join
        left outer join mtl_material_transactions mmt
          on (wdd.transaction_id = mmt.transaction_id)
      
        left outer join mtl_unit_transactions mut
          on (mmt.transaction_id = mut.transaction_id)
      
        join wsh_lookups lk
          on (lk.LOOKUP_CODE = wdd.RELEASED_STATUS)
      --SN# join
      
        left outer join MTL_MATERIAL_TRANSACTIONS_TEMP mmtt
          on (mmtt.MOVE_ORDER_LINE_ID = wdd.MOVE_ORDER_LINE_ID and
             mmtt.ORGANIZATION_ID = wdd.ORGANIZATION_ID and
             mmtt.PROCESS_FLAG = 'Y')
      
        left outer join mtl_serial_numbers_temp msnt
          on (msnt.TRANSACTION_TEMP_ID = mmtt.TRANSACTION_TEMP_ID)
      
       where lk.LOOKUP_TYPE = 'PICK_STATUS'
         and lk.MEANING in
             ('Released to Warehouse', 'Staged/Pick Confirmed')
            
         and nvl(ol.cancelled_flag, 'N') = 'N'
         and ol.open_flag = 'Y'
         and ol.booked_flag = 'Y'
         and ol.flow_status_code not in
             ('AWAITING_FULFILLMENT',
              'INVOICED',
              'INVOICE_HOLD',
              'INVOICE_INCOMPLETE')
         and ol.fulfilled_flag is null
         and wnd.creation_date between
             to_date(p_date_from, 'yyyy/mm/dd HH24:MI:SS') and
             to_date(p_date_to, 'yyyy/mm/dd HH24:MI:SS')
            -- and wdd.organization_id = p_org_id
         and ol.SHIP_FROM_ORG_ID = p_org_id
         and wnd.delivery_id = nvl(p_delivery_id, wnd.delivery_id)
            --Join to contact name
         and nvl(ship_rel.object_id, -1) = nvl(ship_acct.party_id, -1)
            --Join email info
         AND oh.sold_to_org_id = cust_acct.cust_account_id
         AND CUST_ACCT.PARTY_ID = PARTY.PARTY_ID
            --Get order type
         and ott.transaction_type_id = oh.order_type_id ----
            --Get item type
         and micv.organization_id = 84 ----
         and micv.category_set_name = 'ITEM_TYPE' ----
         and not exists
       (select 'X'
                from XXSHIP.XXSTAR_ROR_REL_SHIPMENTS_TBL XRSS
               WHERE XRSS.DELIVERY_DETAIL_ID = WDD.DELIVERY_DETAIL_ID
                 AND XRSS.DELIVERY_NUMBER = wnd.delivery_id)
       order by release_date, order_number, line_number;
  
    -- Cursor Staging Table
    cursor c_shpmts_stgng is
      select XRSS.*
        from XXSHIP.XXSTAR_ROR_REL_SHIPMENTS_TBL XRSS
       where XRSS.status = 'NEW'
         and XRSS.delivery_creation_date between
             to_date(p_date_from, 'yyyy/mm/dd HH24:MI:SS') and
             to_date(p_date_to, 'yyyy/mm/dd HH24:MI:SS')
         and XRSS.order_line_ship_from_org_id = p_org_id
         and XRSS.wnd_delivery_id =
             nvl(p_delivery_id, XRSS.wnd_delivery_id)
       order by XRSS.ORDER_NUMBER, XRSS.DELIVERY_NUMBER ASC;
  
    l_sftp_request_id      number;
    l_mntr_sftp_request_id number;
    p_conc_request_id      number;
    p_filename_to          varchar2(100);
    l_num_value            number;
    l_hdr_prntd            varchar2(1) := 'N';
    l_upload_file          varchar2(1);
    v_ex_request_id        number;
    l_user_id              number;
    xml_layout             boolean;
  
  BEGIN
    fnd_file.put_line(fnd_file.log,
                      '------------------ START ------------------ ');
    out_errnum_n := 0;
    -- Populate Staging Table
    for r_shpmts in c_shpmts loop
      begin
        insert into "XXSHIP"."XXSTAR_ROR_REL_SHIPMENTS_TBL"
          select TO_DATE(r_shpmts.release_date, 'DD-MM-YYYY'),
                 r_shpmts.delivery_number,
                 r_shpmts.ship_via,
                 TO_DATE(r_shpmts.start_ship_date, 'DD-MM-YYYY'),
                 r_shpmts.ship_to_name,
                 r_shpmts.ship_to_id,
                 r_shpmts.ship_address1,
                 r_shpmts.ship_address2,
                 r_shpmts.ship_address3,
                 r_shpmts.ship_city,
                 r_shpmts.ship_postal_code,
                 r_shpmts.ship_country,
                 r_shpmts.line_number,
                 r_shpmts.item_number,
                 r_shpmts.shipped_quantity,
                 r_shpmts.order_number,
                 r_shpmts.currency_code,
                 r_shpmts.unit_selling_price,
                 r_shpmts.Business_unit,
                 r_shpmts.subinventory_code,
                 r_shpmts.inco_terms,
                 r_shpmts.freight_terms,
                 r_shpmts.bill_to_name,
                 r_shpmts.bill_to_id,
                 r_shpmts.bill_address1,
                 r_shpmts.bill_address2,
                 r_shpmts.bill_address3,
                 r_shpmts.bill_city,
                 r_shpmts.bill_postal_code,
                 r_shpmts.bill_country,
                 r_shpmts.del_to_name,
                 r_shpmts.del_to_id,
                 r_shpmts.del_address1,
                 r_shpmts.del_address2,
                 r_shpmts.del_address3,
                 r_shpmts.del_city,
                 r_shpmts.del_postal_code,
                 r_shpmts.del_country,
                 r_shpmts.Extended_price,
                 r_shpmts.cust_po_number,
                 r_shpmts.line_comments,
                 r_shpmts.TRAD_PARTN_ID,
                 r_shpmts.batch_num,
                 r_shpmts.serial_number,
                 r_shpmts.shp_cntc_name,
                 r_shpmts.shp_cntc_email,
                 r_shpmts.ship_cntc_phone,
                 r_shpmts.ship_to_state,
                 r_shpmts.OrderQtyUOM,
                 r_shpmts.OrdersStatusCode,
                 r_shpmts.OrderType,
                 r_shpmts.ItemTypeCategory,
                 
                 fnd_profile.value('USER_ID'), --CREATED_BY
                 SYSDATE, --CREATION_DATE
                 SYSDATE, --LAST_UPDATE_DATE
                 fnd_profile.value('USER_ID'), --LAST_UPDATED_BY
                 fnd_profile.value('LOGIN_ID'), --LAST_UPDATE_LOGIN
                 fnd_global.conc_request_id, --fnd_profile.value('CONCURRENT_REQUEST_ID'), --REQUEST ID
                 
                 NULL, --PROCESSING_FLAG
                 NULL, --ERROR_DESCRIPTION
                 NULL, --COMMENTS
                 NULL, --DATE_FUTURE1
                 NULL, --DATE_FUTURE2
                 NULL, --QTY_FUTURE
                 NULL, --ATTRIBUTE1
                 NULL, --ATTRIBUTE2
                 NULL, --ATTRIBUTE3
                 NULL, --ATTRIBUTE4
                 NULL, --ATTRIBUTE5
                 'NEW', --STATUS --UPLOADED or NO UPLOADED
                 NULL, --SENTTIMESTAMP
                 
                 r_shpmts.delivery_detail_id, --DELIVERY_DETAIL_ID
                 
                 -- Values to Filter Staging Table
                 r_shpmts.delivery_creation_date,
                 r_shpmts.order_line_ship_from_org_id,
                 r_shpmts.wnd_delivery_id
            from dual;
      
      exception
        when others then
          out_errbuf_s := 'UNEXP_ERR in  XXSTAR_ROR_REL_SHIPMENTS: ' ||
                          SQLERRM;
          out_errnum_n := 1;
          fnd_file.put_line(fnd_file.log, out_errbuf_s);
          dbms_output.put_line(out_errbuf_s);
      end;
    end loop;
    -- End Populate Staging Table
  
    -- Query Staging Table to create file
    for r_shpmts_stgng in c_shpmts_stgng loop
      begin
        if l_hdr_prntd = 'N' then
          -------------------------------Set File Name --------------------------------------
          p_filename_to := '850_SOREL_' ||
                           to_char(sysdate, 'ddmmyy_hhmiss') || '.txt';
        
          -----------------------------Create  file -----------------------------------
        
          FND_FILE.put_line(FND_FILE.OUTPUT,
                            'Release Date' || '|' ||
                            'Scheduled Ship Week Ending' || '|' ||
                            'Order Number' || '|' || 'Ship Via' || '|' ||
                            'Ship To Id' || '|' || 'Ship To Name' || '|' ||
                            'Ship Address1' || '|' || 'Ship Address2' || '|' ||
                            'Ship Address3' || '|' || 'Ship City' || '|' ||
                            'Ship Postal Code' || '|' || 'Ship Country' || '|' ||
                            'Line Number' || '|' || 'Item Number' || '|' ||
                            'Ship Quantity' || '|' || 'Subinventory' || '|' ||
                            'Inco Terms' || '|' || 'Freight Terms' || '|' ||
                            'Business Unit' || '|' || 'Currency Code' || '|' ||
                            'Unit Selling Price' || '|' ||
                            'Delivery Number' || '|' || 'Deliver To Name' || '|' ||
                            'Deliver To Id' || '|' || 'Deliver Address1' || '|' ||
                            'Deliver Address2' || '|' || 'Deliver Address3' || '|' ||
                            'Deliver City' || '|' || 'Deliver Postal Code' || '|' ||
                            'Deliver Country' || '|' || 'Bill To Name' || '|' ||
                            'Bill To Id' || '|' || 'Bill Address1' || '|' ||
                            'Bill Address2' || '|' || 'Bill Address3' || '|' ||
                            'Bill City' || '|' || 'Bill Postal Code' || '|' ||
                            'Bill Country' || '|' || 'Extended Price' || '|' ||
                            'Customer PO' || '|' || 'Line Comments' || '|' ||
                            'TradingPartnerId' || '|' || 'Batch Number' || '|' ||
                            'Serial Number' || '|' ||
                            'Ship to contact Name' || '|' ||
                            'Ship to contact email' || '|' ||
                            'Ship to contact Phone' || '|' ||
                            'Ship to State' || '|' || 'OrderQtyUOM' || '|' ||
                            'OrdersStatusCode' || '|' || 'OrderType' || '|' ||
                            'ItemTypeCategory');
        end if;
        --Generate data file from Staging Table
        FND_FILE.put_line(FND_FILE.OUTPUT,
                          TO_CHAR(r_shpmts_stgng.release_date, 'DD-MM-YYYY') || '|' || --
                          TO_CHAR(r_shpmts_stgng.start_ship_date,
                                  'DD-MM-YYYY') || '|' || --
                          r_shpmts_stgng.order_number || '|' || --
                          r_shpmts_stgng.ship_via || '|' || --
                          r_shpmts_stgng.ship_to_id || '|' || --
                          r_shpmts_stgng.ship_to_name || '|' || --
                          r_shpmts_stgng.ship_address1 || '|' || --
                          r_shpmts_stgng.ship_address2 || '|' || --
                          r_shpmts_stgng.ship_address3 || '|' || --
                          r_shpmts_stgng.ship_city || '|' || --
                          r_shpmts_stgng.ship_postal_code || '|' || --
                          r_shpmts_stgng.ship_country || '|' || --
                          r_shpmts_stgng.order_line_number || '|' || --
                          r_shpmts_stgng.item_number || '|' || --
                          r_shpmts_stgng.shipped_quantity || '|' || --
                          r_shpmts_stgng.subinventory_code || '|' || --
                          r_shpmts_stgng.inco_terms || '|' || --
                          r_shpmts_stgng.freight_terms || '|' || --
                          r_shpmts_stgng.Business_unit || '|' || --
                          r_shpmts_stgng.currency_code || '|' || --
                          r_shpmts_stgng.unit_selling_price || '|' || --
                          r_shpmts_stgng.delivery_number || '|' || --
                          r_shpmts_stgng.del_to_name || '|' || --
                          r_shpmts_stgng.del_to_id || '|' || --
                          r_shpmts_stgng.del_address1 || '|' || --
                          r_shpmts_stgng.del_address2 || '|' || --
                          r_shpmts_stgng.del_address3 || '|' || --
                          r_shpmts_stgng.del_city || '|' || --
                          r_shpmts_stgng.del_postal_code || '|' || --
                          r_shpmts_stgng.del_country || '|' || --
                          r_shpmts_stgng.bill_to_name || '|' || --
                          r_shpmts_stgng.bill_to_id || '|' || --
                          r_shpmts_stgng.bill_address1 || '|' || --
                          r_shpmts_stgng.bill_address2 || '|' || --
                          r_shpmts_stgng.bill_address3 || '|' || --
                          r_shpmts_stgng.bill_city || '|' || --
                          r_shpmts_stgng.bill_postal_code || '|' || --
                          r_shpmts_stgng.bill_country || '|' || --
                          r_shpmts_stgng.Extended_price || '|' || --
                          r_shpmts_stgng.cust_po_number || '|' || --
                          r_shpmts_stgng.line_comments || '|' || --
                          r_shpmts_stgng.TRAD_PARTN_ID || '|' || --
                          r_shpmts_stgng.batch_number || '|' || --
                          r_shpmts_stgng.serial_number || '|' || --
                          r_shpmts_stgng.shp_cntc_name || '|' || --
                          r_shpmts_stgng.shp_cntc_email || '|' || --
                          r_shpmts_stgng.ship_cntc_phone || '|' || --
                          r_shpmts_stgng.ship_to_state || '|' || --
                          r_shpmts_stgng.OrderQtyUOM || '|' || --
                          r_shpmts_stgng.OrdersStatusCode || '|' || --
                          r_shpmts_stgng.OrderType || '|' || --
                          r_shpmts_stgng.ItemTypeCategory); --
        --Generate data file from Staging Table End
        l_hdr_prntd := 'Y';
      exception
        when others then
          out_errbuf_s := 'UNEXP_ERR in  XXSTAR_ROR_REL_SHIPMENTS: ' ||
                          SQLERRM;
          out_errnum_n := 1;
          fnd_file.put_line(fnd_file.log, out_errbuf_s);
          dbms_output.put_line(out_errbuf_s);
      end;
    end loop;
    -- End Query Staging Table to create file
    l_upload_file     := nvl(p_upload_file, 'N');
    p_conc_request_id := fnd_global.conc_request_id;
    --Upload the file generated and update Staging table
    if l_hdr_prntd = 'Y' then
      if l_upload_file = 'Y' then
        --------- Call Program to sent file generated through sftp ----------
      
        l_sftp_request_id := FND_REQUEST.SUBMIT_REQUEST('XXSTAR',
                                                        'XXSTAR_SFTP_UPLOAD',
                                                        'Star Trac ROR Release Shipments',
                                                        null,
                                                        null,
                                                        --argument starts
                                                        
                                                        --arguments file source
                                                        p_conc_request_id,
                                                        --arguments sftp connection
                                                        p_sftp_credentials,
                                                        --arguments file destination
                                                        p_dir_to,
                                                        p_filename_to,
                                                        
                                                        chr(0),
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '');
      
        --Call program to monitor the Upload Program
        l_mntr_sftp_request_id := FND_REQUEST.SUBMIT_REQUEST('XXSTAR',
                                                             'XXSTAR_MNTR_ROR_RL_SHPMTS_PLDN', --'XXSTAR_MNTR_ROR_RL_SHPMTS_PLD',
                                                             'Star Trac ROR Release Shipments',
                                                             null,
                                                             null,
                                                             --argument starts
                                                             
                                                             --arguments Date From
                                                             p_date_from, --1 Date From
                                                             --arguments Date To
                                                             p_date_to, --2
                                                             --arguments Org Id
                                                             p_org_id, --3
                                                             --arguments Delivery Id
                                                             p_delivery_id, --4
                                                             --arguments Request Id
                                                             l_sftp_request_id, --5
                                                             --arguments Request ID Release Program
                                                             p_conc_request_id, --6
                                                             chr(0),
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '',
                                                             '');
        --Call program to monitor the Upload Program End
      else
        --When no upload file  then update the records picked and already filtered
        --Sent the exception report
        BEGIN
          for r_shpmts_u_stgng in c_shpmts_stgng loop
          
            UPDATE "XXSHIP"."XXSTAR_ROR_REL_SHIPMENTS_TBL"
               SET LAST_UPDATE_DATE  = SYSDATE,
                   LAST_UPDATED_BY   = fnd_profile.value('USER_ID'),
                   LAST_UPDATE_LOGIN = fnd_profile.value('LOGIN_ID'),
                   REQUEST_ID        = p_conc_request_id
             WHERE delivery_detail_id = r_shpmts_u_stgng.delivery_detail_id
               AND STATUS = 'NEW';
          
          end loop;
          commit;
        EXCEPTION
          WHEN OTHERS THEN
            out_errbuf_s := 'UNEXP_ERR in  XXSTAR_ROR_REL_SHIPMENTS: ' ||
                            SQLERRM;
            out_errnum_n := 1;
            fnd_file.put_line(fnd_file.log, out_errbuf_s);
            dbms_output.put_line(out_errbuf_s);
        END;
        --Submit Exception Report
        --------------d--------------
        v_ex_request_id := null;
        l_user_id       := fnd_profile.value('USER_ID');
        begin
          xml_layout      := FND_REQUEST.ADD_LAYOUT('XXSTAR',
                                                    'XXSTAR_ROR_SHPMNTS_STTS',
                                                    'en',
                                                    'US',
                                                    'EXCEL');
          v_ex_request_id := FND_REQUEST.SUBMIT_REQUEST('XXSTAR',
                                                        'XXSTAR_ROR_SHPMNTS_STTS',
                                                        'Exception Report ROR',
                                                        null,
                                                        null,
                                                        --argument starts
                                                        p_conc_request_id, --Request ID
                                                        'REL', --Template Code
                                                        'Y', --Send Email?
                                                        l_user_id, --User ID
                                                        chr(0),
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '');
        exception
          when others then
            fnd_file.put_line(fnd_file.log,
                              'error while submitting exception report  :' ||
                              sqlerrm);
        end;
      
        if nvl(v_ex_request_id, 0) != 0 then
          fnd_file.put_line(fnd_file.log,
                            'Exception report is submitted: ' ||
                            v_ex_request_id);
          commit;
        else
          fnd_file.put_line(fnd_file.log,
                            'Exception report Could not be submited');
        end if;
        --------------d------------
      end if;
    end if;
  
  EXCEPTION
    WHEN OTHERS THEN
      out_errbuf_s := 'UNEXP_ERR in  XXSTAR_ROR_REL_SHIPMENTS: ' || SQLERRM;
      out_errnum_n := 1;
      fnd_file.put_line(fnd_file.log, out_errbuf_s);
      dbms_output.put_line(out_errbuf_s);
  END main;
  ------------
  PROCEDURE mntrng(out_errbuf_s OUT VARCHAR2,
                   out_errnum_n OUT NUMBER,
                   
                   p_date_from   in varchar2,
                   p_date_to     in varchar2,
                   p_org_id      in varchar2,
                   p_delivery_id in varchar2,
                   
                   p_sftp_request_id in number,
                   p_conc_request_id in number) IS
    -- Cursor Staging Table
    cursor c_shpmts_stgng is
      select XRSS.*
        from "XXSHIP"."XXSTAR_ROR_REL_SHIPMENTS_TBL" XRSS
       where XRSS.status = 'NEW'
         and XRSS.delivery_creation_date between
             to_date(p_date_from, 'yyyy/mm/dd HH24:MI:SS') and
             to_date(p_date_to, 'yyyy/mm/dd HH24:MI:SS')
         and XRSS.order_line_ship_from_org_id = p_org_id
         and XRSS.wnd_delivery_id =
             nvl(p_delivery_id, XRSS.wnd_delivery_id);
  
    v_counter       number := 0;
    v_phase_code    varchar2(20) := null;
    v_status_code   varchar2(20) := null;
    v_continue      varchar2(20) := 'Y';
    v_request_id    number;
    v_finished_ok   varchar2(20) := null;
    v_timeout_err   varchar2(20) := null;
    v_ex_request_id number;
    l_user_id       number;
    xml_layout      boolean;
  
  BEGIN
  
    v_request_id := p_sftp_request_id;
  
    WHILE v_continue = 'Y' LOOP
      select PHASE_CODE, STATUS_CODE
        into v_phase_code, v_status_code
        from fnd_concurrent_requests
       where request_id = v_request_id;
    
      if v_phase_code = 'C' then
        if v_status_code = 'C' then
          v_finished_ok := 'Y';
          v_continue    := 'N';
        else
          v_finished_ok := 'N';
          v_continue    := 'N';
        end if;
      end if;
    
      ---Sleep the program to avoid forever loop
      dbms_lock.sleep(60);
      v_counter := v_counter + 1;
      if v_counter > 60 then
        v_timeout_err := 'Y';
        v_continue    := 'N';
      end if;
    
    END LOOP;
  
    if v_finished_ok = 'Y' then
      BEGIN
        --Update rows uploaded from the Staging Table
        for r_shpmts_u_stgng in c_shpmts_stgng loop
        
          UPDATE "XXSHIP"."XXSTAR_ROR_REL_SHIPMENTS_TBL"
             SET STATUS            = 'SENT',
                 SENTTIMESTAMP     = SYSDATE,
                 LAST_UPDATE_DATE  = SYSDATE,
                 LAST_UPDATED_BY   = fnd_profile.value('USER_ID'),
                 LAST_UPDATE_LOGIN = fnd_profile.value('LOGIN_ID'),
                 REQUEST_ID        = p_conc_request_id
           WHERE delivery_detail_id = r_shpmts_u_stgng.delivery_detail_id
             AND STATUS = 'NEW';
        
        end loop;
        commit;
        --Update rows uploaded from the Staging Table End
      EXCEPTION
        WHEN OTHERS THEN
          out_errbuf_s := 'UNEXP_ERR in  XXSTAR_ROR_REL_SHIPMENTS: ' ||
                          SQLERRM;
          out_errnum_n := 1;
          fnd_file.put_line(fnd_file.log, out_errbuf_s);
          dbms_output.put_line(out_errbuf_s);
      END;
    else
      BEGIN
        --Update rows not uploaded from the Staging Table
        for r_shpmts_u_stgng in c_shpmts_stgng loop
        
          UPDATE "XXSHIP"."XXSTAR_ROR_REL_SHIPMENTS_TBL"
             SET LAST_UPDATE_DATE  = SYSDATE,
                 LAST_UPDATED_BY   = fnd_profile.value('USER_ID'),
                 LAST_UPDATE_LOGIN = fnd_profile.value('LOGIN_ID'),
                 ERROR_DESCRIPTION = 'The Star Trac SFTP Upload program failed to upload the release feed file',
                 REQUEST_ID        = p_conc_request_id
           WHERE delivery_detail_id = r_shpmts_u_stgng.delivery_detail_id
             AND STATUS = 'NEW';
        
        end loop;
        commit;
        --Update rows not uploaded from the Staging Table End
      EXCEPTION
        WHEN OTHERS THEN
          out_errbuf_s := 'UNEXP_ERR in  XXSTAR_ROR_REL_SHIPMENTS: ' ||
                          SQLERRM;
          out_errnum_n := 1;
          fnd_file.put_line(fnd_file.log, out_errbuf_s);
          dbms_output.put_line(out_errbuf_s);
      END;
    end if;
  
    --------------d---------
    v_ex_request_id := null;
    l_user_id       := fnd_profile.value('USER_ID');
    begin
      xml_layout      := FND_REQUEST.ADD_LAYOUT('XXSTAR',
                                                'XXSTAR_ROR_SHPMNTS_STTS',
                                                'en',
                                                'US',
                                                'EXCEL');
      v_ex_request_id := FND_REQUEST.SUBMIT_REQUEST('XXSTAR',
                                                    'XXSTAR_ROR_SHPMNTS_STTS',
                                                    'Exception Report ROR',
                                                    null,
                                                    null,
                                                    --argument starts
                                                    p_conc_request_id, --Request ID
                                                    'REL', --Template Code
                                                    'Y', --Send Email?
                                                    l_user_id, --User ID
                                                    chr(0),
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '');
    exception
      when others then
        fnd_file.put_line(fnd_file.log,
                          'error while submitting exception report  :' ||
                          sqlerrm);
    end;
  
    if nvl(v_ex_request_id, 0) != 0 then
      fnd_file.put_line(fnd_file.log,
                        'Exception report is submitted: ' ||
                        v_ex_request_id);
      commit;
    else
      fnd_file.put_line(fnd_file.log,
                        'Exception report Could not be submited');
    end if;
    --------------d---------
  
  END;
  -----------
  function get_unit_sell_price(p_line_id in number) return number is
    l_price_brkd              number := 0;
    l_is_compnnt              varchar(1) := 'N';
    l_unit_cost               number := 0;
    l_ttl_unit_cost           number := 0;
    l_prnt_line_id            number := 0;
    l_prnt_unit_selling_price number := 0;
    l_prcntg_unit_cost        number := 0;
  begin
    begin
      select decode(count(ol.line_id), 1, 'Y', 'N')
        into l_is_compnnt
        from oe_order_lines_all ol
       where ol.line_id = p_line_id
         and ol.component_sequence_id is not null
         and ol.link_to_line_id is not null;
      if l_is_compnnt = 'N' then
        select ol.unit_selling_price
          into l_price_brkd
          from oe_order_lines_all ol
         where ol.line_id = p_line_id;
        return l_price_brkd;
      else
        -- Get Unit Cost  Get KIT's LineId
        select cic.item_cost, ol.link_to_line_id
          into l_unit_cost, l_prnt_line_id
          from oe_order_lines_all ol,
               cst_item_costs     cic,
               cst_cost_types     cct
         where 1 = 1
           and ol.line_id = p_line_id
           and cic.inventory_item_id = ol.inventory_item_id
           and cic.organization_id = ol.ship_from_org_id
           and cic.cost_type_id = cct.cost_type_id
           and cct.cost_type = 'Frozen';
      
        -- Get Total Unit Cost
        select sum(cic.item_cost)
          into l_ttl_unit_cost
          from oe_order_lines_all ol,
               cst_item_costs     cic,
               cst_cost_types     cct
         where 1 = 1
           and ol.link_to_line_id = l_prnt_line_id
           and cic.inventory_item_id = ol.inventory_item_id
           and cic.organization_id = ol.ship_from_org_id
           and cic.cost_type_id = cct.cost_type_id
           and cct.cost_type = 'Frozen';
      
        -- Get % of total cost of the bundle for each component
        select (l_unit_cost / l_ttl_unit_cost)
          into l_prcntg_unit_cost
          from dual;
      
        -- Get Unit Selling Price of the KIT
        select ol.unit_selling_price
          into l_prnt_unit_selling_price
          from oe_order_lines_all ol
         where ol.line_id = l_prnt_line_id;
      
        -- Get Price brkdwn
        select ROUND((l_prnt_unit_selling_price * l_prcntg_unit_cost), 2)
          into l_price_brkd
          from dual;
      
        return l_price_brkd;
      
      end if;
    exception
      when others then
        fnd_file.put_line(fnd_file.log,
                          'Error at get_unit_sell_price function: ' ||
                          sqlerrm || chr(10) || 'so_line_id used : ' ||
                          p_line_id);
        return null;
    end;
  end;

  function gt_shp_mthd(p_item_id in number) return varchar2 as
    l_item_type varchar2(100);
    l_ship_mthd varchar2(100);
  
  begin
    begin
      select lk.category_concat_segs
        into l_item_type
        from (select category_concat_segs, organization_id
                from mtl_item_categories_v
               where inventory_item_id = p_item_id
                 and organization_id = 84
                 and category_set_name = 'ITEM_TYPE') lk,
             (select 84 organization_id from dual) dl
       where lk.organization_id(+) = dl.organization_id;
    
      select flv.ATTRIBUTE2
        into l_ship_mthd
        from fnd_lookup_values flv
       where flv.LOOKUP_TYPE = 'XXSTAR_ROR_RL_SHP_DFLT_SHP_VIA'
         and flv.ENABLED_FLAG = 'Y'
            --and flv.ATTRIBUTE1 = l_item_type
         and NVL(flv.ATTRIBUTE1, '@') = NVL(l_item_type, '@')
         and flv.START_DATE_ACTIVE <= sysdate
         and sysdate <= nvl(flv.END_DATE_ACTIVE, sysdate + 1);
    
      return l_ship_mthd;
    
    exception
      when others then
        fnd_file.put_line(fnd_file.log,
                          'Error at gt_shp_mthd function : ' || sqlerrm ||
                          chr(10) || 'Inventory_item_id used : ' ||
                          p_item_id);
        return null;
    end;
  end;

  function gt_chk_wrrnt_so(p_order_type         in varchar2,
                           p_unit_selling_price in number) return number as
    l_wrrnt_so number;
  
  begin
    begin
    
      select count(flv.MEANING)
        into l_wrrnt_so
        from fnd_lookup_values flv
       where flv.LOOKUP_TYPE = 'XXSTAR_ROR_REL_SHPMNTS_DFLT_PR'
         and flv.ENABLED_FLAG = 'Y'
         and flv.MEANING = p_order_type
         and flv.START_DATE_ACTIVE <= sysdate
         and sysdate <= nvl(flv.END_DATE_ACTIVE, sysdate + 1);
    
      if (l_wrrnt_so = 1) and (p_unit_selling_price = 0) then
        return 1;
      else
        return 0;
      end if;
    
    exception
      when others then
        fnd_file.put_line(fnd_file.log,
                          'Error at gt_chk_wrrnt_so: ' || sqlerrm ||
                          chr(10) || 'Order Type used : ' || p_order_type ||
                          chr(10) || 'Unit Selling Price used : ' ||
                          p_unit_selling_price);
        return 0;
    end;
  end;

  function f_get_state(p_ship_to_site_code_id in number) return varchar2 is
    lc_state varchar2(3);
  begin
    lc_state := '';
  
    begin
      select substr(t.state, 1, 3)
        into lc_state
        from hz_cust_site_uses_all   site,
             hz_party_sites          party_site,
             hz_locations            loc,
             hz_cust_acct_sites_all  acct_site,
             xxstar_state_coversions t
       where site.site_use_code = 'SHIP_TO'
         and upper(loc.COUNTRY) = upper(t.country)
         and upper(loc.STATE) = upper(t.state_name)
         and site.cust_acct_site_id = acct_site.cust_acct_site_id
         and acct_site.party_site_id = party_site.party_site_id
         and party_site.location_id = loc.location_id
         and site.site_use_id = p_ship_to_site_code_id;
      return lc_state;
    exception
      when others then
        fnd_file.put_line(fnd_file.log,
                          'Error at getting Ship To State code from then xxstar_state_coversions table  : ' ||
                          sqlerrm || chr(10) ||
                          'ship_to_site_code_id used : ' ||
                          p_ship_to_site_code_id);
    end;
  
    begin
      select substr(loc.state, 1, 3)
        into lc_state
        from hz_cust_site_uses_all  site,
             hz_party_sites         party_site,
             hz_locations           loc,
             hz_cust_acct_sites_all acct_site
       where site.site_use_code = 'SHIP_TO'
         and site.cust_acct_site_id = acct_site.cust_acct_site_id
         and acct_site.party_site_id = party_site.party_site_id
         and party_site.location_id = loc.location_id
         and site.site_use_id = p_ship_to_site_code_id;
    exception
      when others then
        fnd_file.put_line(fnd_file.log,
                          'Error at getting Ship To State code  from the hz_locations table: ' ||
                          sqlerrm || chr(10) ||
                          'ship_to_site_code_id used : ' ||
                          p_ship_to_site_code_id);
    end;
  
    return lc_state;
  end;

end XXSTAR_ROR_REL_SHIPMENTS_N2;
/
